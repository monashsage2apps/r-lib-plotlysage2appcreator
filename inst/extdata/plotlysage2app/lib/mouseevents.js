function SAGE2_MouseEvent (sage2App) {

	this.sage2App = sage2App;
	
	this.clientID = parseInt(getParameterByName("clientID")) || 0;
	console.log('running on display: ' + this.clientID);
	this.offset = {
		x : sage2App.config.displays[this.clientID].column * sage2App.config.resolution.width,
		y : sage2App.config.displays[this.clientID].row * sage2App.config.resolution.height
	};
	
	this.sage2App.execmouseevent = this.execevent.bind(this);
	
	this.prevelem;
	
	this.prevmousepos;
	
}

SAGE2_MouseEvent.prototype.handleEvent = function(eventType, position, user_id, data, date){
        var type;
		var changed = false;

        if(this.prevmousepos === undefined)
            this.prevmousepos = {
                x : position.x,
                y : position.y
            };

        /**
         * handle mouse events
         */

        var e = {
            bubbles: true,
            cancelable: (type != "mousemove"),
            //view: window,
            detail: 0,
            screenX: position.x,
            screenY: position.y,
            clientX: position.x,
            clientY: position.y,
            ctrlKey: false,
            altKey: false,
            shiftKey: false,
            metaKey: false,
            button: 0,
            relatedTarget: undefined

        };

        if(eventType == "pointerMove") {
            e.button = this.buttonstatus ;
            e.buttons = this.buttonstatus;
            if(this.buttonstatus != 0)
                type = 'drag';
            else
                type = 'mousemove';
			
			
        }
        if(eventType == "pointerPress") {
            type = 'mousedown';
            e.button = 1;
            e.buttons = 1;
            this.buttonstatus = e.button;

        }
        if(eventType == "pointerRelease") {
            if(this.preveventtype == "pointerPress")
                type = 'click';
            else
                type = 'mouseup';
            e.button = 0;
            e.buttons = 0;
            this.buttonstatus = 0;

        }
		if(eventType === "pointerScroll") {
			
		}

		
        var pointer = document.getElementsByClassName("pointerItem");

        for (i = 0; i < pointer.length; i++) {
            pointer[i].style['display'] = "none";
        }

        var posx = position.x - this.offset.x + this.sage2App.sage2_x ;
        var posy = position.y - window.ui.offsetY - this.offset.y + this.sage2App.sage2_y;

        var elem = document.elementFromPoint(posx, posy );

        for (i = 0; i < pointer.length; i++) {
            pointer[i].style['display'] = "block";
        }

		if(elem !== undefined && elem != null) {
			var rect = elem.getBoundingClientRect();
			//console.log(posx + ' ' + posy + ' ' + elem)
			var curPathElem = elem;
			var path = [];
			var i = 0;
			while(curPathElem !== document) {
				var idx = 0;
				var parent = curPathElem.parentNode;
				if(!parent) {
					console.log('parent null');
					return;
				}
				var children = curPathElem.parentNode.childNodes;
				for(idx = 0; idx < children.length; idx++) {
					if(children[idx] == curPathElem)
						break;

				}
				path[i++] = {elemName : curPathElem.tagName, className : curPathElem.className, idx : idx};
				curPathElem = curPathElem.parentNode;
			}
			
            var brdata ={mouseeventtype : type, mouseeventdata : e, targetelementidxpath : path};
			
            if(elem != this.prevelem) {
                var tempeventtype = brdata.mouseeventtype;

                brdata.mouseeventtype = 'mouseover';
                this.sage2App.broadcast('execmouseevent', brdata);
                brdata.mouseeventtype = 'mouseenter';
                this.sage2App.broadcast('execmouseevent', brdata);                
                brdata.mouseeventtype = tempeventtype;
            }
            if(brdata.mouseeventtype == 'click') {
                var tempeventtype = brdata.mouseeventtype;
                brdata.mouseeventtype = 'mouseup';
                this.sage2App.broadcast('execmouseevent', brdata);
                brdata.mouseeventtype = tempeventtype;
            }

            this.sage2App.broadcast('execmouseevent', brdata);
            
            this.preveventtype = eventType;

		}
        this.prevmousepos = {
            x : position.x,
            y : position.y
        };

        this.prevelem = elem;
		
		
	
	}
	
	
SAGE2_MouseEvent.prototype.execevent = function(data) {
        //console.log(JSON.stringify(data));

        var targetelement = document;
        var length = data.targetelementidxpath.length - 1;
        for(var i = length; i >=0; i--) {
            var idx = data.targetelementidxpath[i].idx;
            targetelement = targetelement.childNodes[idx];
        }
        //readjust
        var posx = data.mouseeventdata.screenX - this.offset.x +  this.sage2App.sage2_x;
        var posy = data.mouseeventdata.screenY + window.ui.offsetY - this.offset.y + this.sage2App.sage2_y;
        data.mouseeventdata.screenX = posx;
        data.mouseeventdata.screenY = posy;
        data.mouseeventdata.clientX = posx;
        data.mouseeventdata.clientY = posy;
        //console.log("[execmouseevent] x,y : " + posx + ", " + posy + " sagewindowXY: " + this.sage2_y);
		
		var me = new MouseEvent(
			data.mouseeventtype,
			data.mouseeventdata
		);
	
		targetelement.dispatchEvent(me);
}
