//
// SAGE2 application: plotlydemo
// by: Kadek Ananta <ananta.satriadi@gmail.com>
// This is application main js template for rsage2 package in R
// Copyright (c) 2015
//

var %name% = SAGE2_App.extend( {
	init: function(data) {
		// Create div into the DOM
		this.SAGE2Init("div", data);
		// Set the background to black

		
		this.date = data.date;
		this.buttonstatus = 0; // pressed mouse button (-1 no button pressed)
		

		this.element.style.backgroundColor = '#fff';

		// move and resize callbacks
		this.resizeEvents = "onfinish";
		this.moveEvents   = "onfinish";

		
		this.lastTime = performance.now();
		// switch for rotate / pan control
		this.plotcontroltype = 1;
		
		this.sage2mouseevent = new SAGE2_MouseEvent(this);

		// SAGE2 Application Settings
		//

		this.viewerid = "%name%";
		this.plotlyPlot = document.createElement('div');
		this.initPlotArea(this.viewerid);
		
		this.controls.addButton({ label: "Rotate", identifier: "rotate", position: 4 });
		this.controls.addButton({ label: "Pan", identifier: "pan", position: 8 });
		this.controls.addButton({ label: "Reset View", identifier: "resetview", position: 12 });
		
		this.controls.finishedAddingControls();

		this.enableControls = true;		
		
	},

	initPlotArea: function(id){
		this.element.style.fontFamily="sans-serif";
	
		var textnode = document.createElement('h3');
		var description = document.createElement('p');
		textnode.innerHTML = "%title%";
		description.innerHTML = "%description%";
		
		var content = document.createElement("div");
		content.style.display = "table";
		content.style.width="100%";
		content.style.height="100%";
		
		this.header = document.createElement("div");
		this.header.style.display = "table-row";
		this.header.style.backgroundColor = "#DDD";
		this.header.appendChild(textnode);
		this.header.appendChild(description);
		
		this.plotContainer = document.createElement('div');
		
		this.plotContainer.style.display = "table-row";
		
		this.plotContainer.style.width = "100%";
		this.plotContainer.style.height = "100%";
		this.plotContainer.appendChild(this.plotlyPlot);
		
		content.appendChild(this.header);
		content.appendChild(this.plotContainer);
		
		this.element.appendChild(content);
		
		
		this.plot(this.plotlyPlot);		
	},
	
	plot: function(viewer){
		data = %data% ;
		layout = %layout% ;
		Plotly.plot(viewer, data, layout);
	},
	
	load: function(date) {
		if(this.plotlyPlot !== undefined )
			this.setPlotCamera(this.state, true);
	},

    draw: function(date) {


    },

	quit: function() {
		
		if(this.plotlyPlot !== undefined) {
			var fulllayout = this.plotlyPlot._fullLayout;
			
			var _scene = this.plotlyPlot._fullLayout.scene._scene;
			if(_scene.glplot !== undefined)
				_scene.glplot.dispose();
		}
	},
	
	resize: function(date) {
		var newSize = {
			width : this.element.clientWidth,
			height : this.element.clientHeight - this.header.clientHeight,
		};
		Plotly.relayout(document.getElementById(this.viewerid), newSize);
	},

    setPlotCamera : function(data, remote) {
        if(!isMaster || (remote !== undefined && remote == true)){
			if(this.plotlyPlot !== undefined ){
				//console.log('setPlotCamera: slave setting camera ' + JSON.stringify(data.cameraObj.eye));
				var _scene = this.plotlyPlot._fullLayout.scene._scene;
				_scene.setCamera(data.cameraObj);
			}
        }  
    },
	event: function(eventType, position, user_id, data, date) {
        var type;
		var changed = false;
		if(this.plotlyPlot !== undefined) {
			var _scene = this.plotlyPlot._fullLayout.scene._scene;
			var cam = _scene.camera;
		}
		/**
		 * Handle UI input
		 */
		if(eventType == "widgetEvent") {

			switch(data.identifier) {
				case "rotate":
					this.plotcontroltype = 1;

					break;
				case "pan":
					this.plotcontroltype = 2;

					break;
				case "resetview":
					
					_scene.setCameraToDefault();
					break;
			}

			return;
		}


        if(eventType == "pointerMove") {
			
			if(this.buttonstatus == 1 ){
				if(isMaster && this.plotlyPlot !== undefined){
		
					//console.log('master is rotating plot');
					this.lastTime = performance.now();
					//this.lastTime += 10;
					
					//console.log('lastTime: ' + this.lastTime + ', dx / dy : ' + data.dx + ' ' + data.dy);
					if(this.plotcontroltype == 1)
						cam.view.rotate(this.lastTime, -0.001 * data.dx, 0.001 * data.dy, 0.0);
					else if (this.plotcontroltype == 2) {
						cam.view.pan(this.lastTime, -0.001 * data.dx * cam.distance, 0.001 * data.dy * cam.distance, 0.0);
					}
					changed = true;
				} else {
					return;
				}
				
			}
        }
        if(eventType == "pointerPress") {
            this.buttonstatus = 1;

        }
        if(eventType == "pointerRelease") {
            this.buttonstatus = 0;

        }
		if(eventType === "pointerScroll") {
			if(isMaster && this.plotlyPlot !== undefined){
				this.lastTime = performance.now();
				cam.view.pan(this.lastTime, 0, 0, 0.001 * data.wheelDelta);
				changed = true;
			} else {
				return;
			}
		}

		if(changed) {
			var cameraObj = _scene.getCamera();
			//console.log('master sending broadcast ' + JSON.stringify(cameraObj.eye));
			this.state.cameraObj = cameraObj;
			this.refresh();
			this.broadcast('setPlotCamera', {cameraObj:cameraObj});
			
			return;
		}		

		this.sage2mouseevent.handleEvent(eventType, position, user_id, data, date);
    }


});
