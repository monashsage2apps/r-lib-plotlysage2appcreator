% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/generate_sage2_app.R
\name{generate_app_main_js}
\alias{generate_app_main_js}
\title{Generate application main js}
\usage{
generate_app_main_js(app_main_js, app, plot_widget)
}
\arguments{
\item{app_main_js}{= "character" path of application main js}
}
\description{
Modify default application.js based on application name
}
\keyword{application}
\keyword{js,}
\keyword{sage2}

