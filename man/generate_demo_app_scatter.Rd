% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/generate_basic_demo.R
\name{generate_demo_app_scatter}
\alias{generate_demo_app_scatter}
\title{Generate demo app scatter}
\usage{
generate_demo_app_scatter()
}
\description{
Generate plotly demo sage2 application
}
\keyword{application}
\keyword{plotly,}
\keyword{sage2}

